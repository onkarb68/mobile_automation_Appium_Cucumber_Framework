# Getting started with mobile Appium Automation testing with Cucumber , Extent Report & appium libraries

## Automation setup to get started
	Git: Simply clone below project and import in Eclipse. voila!! And you are great to go.. 

    git clone https://gitlab.com/onkarb68/mobile_automation_Appium_Framework.git
    cd mobile_automation_Appium_Framework
	
# Project overview

	This project gives you a basic project setup, along with some sample tests and supporting classes. 
	The starter project shows , how one can validate, automate any mobile application to end using appium libraries

## Test Scenarios covered for Amazon mobile application to test and automate mobile app. Details test step can be seen in cucumber feature file.
	If we would have access to Jira application to author test scenario, test cases , create test cycle to execution. This sounds like a great test artefact one can have. Here, we have used Extent report as test automation artefact for reference. 

	Scenario Outline: Verify  Search functionality of Amazon till product lanading page
	Given User navigates to Amazon app homescreen
	When User click on continue button to proceed as a default Amazon logged-in user
	And User search for mobile model "<MobileName>"
	And User click on mobile model from search result
	Then User verifies the product details on product landing page
	Examples: 
      | MobileName |
      | Apple iPhone 11 Pro Max (256GB) - Midnight Green  |
	  
## Type of Framework: 
	In our project,  have implemented Data-driven Framework by using Page Object Model design pattern with Page Factory.
	  
## 1.Test Base Class: 
	Test Base class (BaseClass.java) deals with all the common functions used by all the pages. E.g. Get webelement, scroll, click & all basic UI actions wrapper
 
	Location: src/main/java/Utility/Baseclass
			........->This class file will have all the frequently used UI actions wrapper in entire appium framework. The advantage of doing this is easy to maintain single file VS multiple files.
			e.g. In future, if project business demands to automate mobile app using latest version of appium libraries or any library, we only need to change this single java file rest all files will remain untouched. This kind of beauty and autonomy this framework brings on the table. 
	
	Note: In current Baseclass, remaining Selenium wrappers can be developed as a ongoing process when respective functionality needs new wrappers to be developed for its automation. Currently have developed only those wrapper which are sufficient for demonstration purposes. 

## 2.How & Where to write feature files?
	Location:  ./Features/Amazon_Search.feature
	
	Gherkin uses a set of special keywords to give structure and meaning to executable specifications. 
	Keywords:
		The primary keywords are:

			1.Feature
			2.Given, When, Then, And, But for steps (or *)
			3.Background
			4.Scenario Outline (or Scenario Template)
			5.Examples (or Scenarios)
			
		There are a few secondary keywords as well:

			1.""" (Doc Strings)
			2.| (Data Tables)
			3.@ (Tags)
			4.# (Comments)
	Example:
	Scenario Outline: Verify  Search functionality of Amazon till product lanading page
	Given User navigates to Amazon app homescreen
	When User click on continue button to proceed as a default Amazon logged-in user
	And User search for mobile model "<MobileName>"
	And User click on mobile model from search result
	Then User verifies the product details on product landing page
	Examples: 
      | MobileName |
      | Apple iPhone 11 Pro Max (256GB) - Midnight Green  |
	  
## 3.Where to write step definition? 
		The glue code is responsible for orchestrating calls to a layer of more business-focused classes, which perform the actual REST calls.
	
	Location: src/test/java/StepDefination/AmazonSearch

## 4.POM:How to group reusable feature page specific components?
	Location: src/main/java/PageObject/ProductScreen
		The actual REST calls are performed using Baseclass in the action classes, like `ProductScreen` here. 
	These methods e.g. getProductName(WebElement ele) use internally consuming core http calls from Baseclass various reusable methods Utility. This will have reusable UI action specific components or methods which can be consumed anywhere, which helps to make Automation code more modular.
	E.g. Lets consider "getProductName(WebElement ele)" method. In case, its implementation gets changed in application. We need to only change in single file, sigle method, so your entire automation code will not get impacted. This is the great modularity it provides.
	
	As per the Page Object Model, have maintained a class for every web page. Each web page has a separate class and that class holds the functionality and members of that web page. Separate classes for every individual test.
	
	public static String getProductName(WebElement ele) {
		WebElement product = baseClass.getElement(ele);
		baseClass.retryingFindElement(product);
		return product.getText();
	}

## 5.Data driven testing:	
	Location: src/test/java/TestData/ProductsData
	
	To store the standard data for data driven testing, TestData package is used. We can create page wise classes to store respective data.
	Apart from that we have used Scenario outline also Data tables can be used. 
	
	Scenario Outline: Verify  Search functionality of Amazon till product lanading page
	Given User navigates to Amazon app homescreen
	When User click on continue button to proceed as a default Amazon logged-in user
	And User search for mobile model "<MobileName>"
	And User click on mobile model from search result
	Then User verifies the product details on product landing page
	Examples: 
      | MobileName |
      | Apple iPhone 11 Pro Max (256GB) - Midnight Green  |
	  
## 6.Packages: 
	Have separate packages for Pages and Tests. All the web page related classes & framework level reusable utility comes under the src/main/java package and all the tests related classes come under src/test/java package.

## 7.Properties file:
	This file (mobile.peoperties) stores the information that remains static throughout the framework such as app-specific information, screenshots path, etc.

	All the details which change as per the environment and authorization such as URL, Login Credentials are kept in the config.properties file. Keeping these details in a separate file makes it easy to maintain.

## 8.Hooks:
	Location: src/main/java/Utility/Hooks
	Hooks are blocks of code that can run at various points in the Cucumber execution cycle. They are typically used for setup and teardown of the environment before and after each scenario
	Have handled android driver instantiation, loading capabilities  etc in Before hook & tear down in after hook.
	
## 9.Cucumber Runner:
	Location: src/main/java/TestRunner/Testrunner
	Cucumber is a JUnit extension. It is launched by running JUnit from your build tool or your IDE.
	
## 10.How to generate Test execution  artifacts , reports?
	
	This is again an awesome plugin that is built on Extent Report specially for Cucumber along with Extent report.
	
		Location:  ./ExtentReports/cucumber-reports/report.html
		
## 11.Logger(Log4j):
		Implemented Logger(log4j library) for quick debugging and RCA purpose
		Location:demoApplication.log

## 12.Gitlab CI runner Pipeline
	
	GitLab CI (Continuous Integration) service is a part of GitLab that build and test the software whenever developer pushes code to application.
	So whenever, Git push events happens, implicitly gitlab CI pipeline job gets triggered which would decide Pull request merge based on it pipelines execution status. 

	.gitlab-ci.yml
	
		demo_job_1:
     tags:
       - ci
     script:
      - mvn clean verify
	

