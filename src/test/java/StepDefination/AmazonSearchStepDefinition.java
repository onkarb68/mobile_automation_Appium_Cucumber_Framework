package StepDefination;

import org.apache.log4j.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import PageObject.HomeScreen;
import PageObject.LoginScreen;
import PageObject.ProductScreen;
import PageObject.SearchScreen;
import TestData.ProductsData;
import Utility.BaseClass;
import Utility.Hooks;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class AmazonSearchStepDefinition {
	Logger logger = Logger.getLogger("AmazonSearch");
	public static WebDriver driver;
	BaseClass baseClass = new BaseClass(driver);
	ProductScreen productScreen;
	
	@Given("^User navigates to Amazon app homescreen$")
	public void user_open_Amazon_app() throws Throwable {

		logger.info("start:User navigates to Amazon app homescreen");
		driver = Hooks.driver;
		// Initialization
		PageFactory.initElements(driver, LoginScreen.class);
		logger.info("Page factory intilaised");
		
		WebElement logo = baseClass.getElement(LoginScreen.amazonLogo);
		logger.info("Logo is displayed on login screen");
		baseClass.retryingFindElement(logo);
	}

	@When("^User click on continue button to proceed as a default Amazon logged-in user$")
	public void user_click_continue() throws Throwable {

		WebElement continueButton = baseClass.getElement(LoginScreen.continueButton);
		baseClass.retryingFindElement(continueButton);
		baseClass.retryingClick(continueButton);
		logger.info("User clicked on continue button to proceed as a default Amazon logged-in user");
	}

	@And("^User search for mobile model \"([^\"]*)\"$")
	public void user_search_for_mobile_model(String mobileModel) throws Throwable {
		// Initialization
		PageFactory.initElements(driver, HomeScreen.class);
		WebElement searchBar = baseClass.getElement(HomeScreen.searchBar);
		baseClass.retryingFindElement(searchBar);
		searchBar.clear();
		searchBar.sendKeys(mobileModel);
		baseClass.retryingClick(searchBar);		
		logger.info("User entered mobile model in search bar");

		WebElement firstResult = baseClass.getElement(HomeScreen.firstResult);
		baseClass.retryingFindElement(firstResult);
		baseClass.retryingClick(firstResult);
		logger.info("User clicked on appropraite mobile model out of all searched resultes");
		logger.info("Mobile model is displayed after User search for mobile model");
	}

	@And("^User click on mobile model from search result$")
	public void user_click_Model() throws Throwable {

		// Initialization
		PageFactory.initElements(driver, SearchScreen.class);
		WebElement product = baseClass.getElement(SearchScreen.product);
				
		baseClass.retryingFindElement(product);
		baseClass.retryingClick(product);
		
		logger.info("User clicked on mobile model from search result");
	}
	
	@Then("^User verifies the product details on product landing page$")
	public void user_verifies_Model() throws Throwable {
		
		// Initialization
		PageFactory.initElements(driver, ProductScreen.class);
		productScreen=new ProductScreen(driver);
		String productName=ProductScreen.getProductName(ProductScreen.productID);
		Assert.assertEquals(ProductsData.productName,productName,"User have not laneded to corect device");
	
		logger.info("User verifies the product details on product landing page");
	}
}
