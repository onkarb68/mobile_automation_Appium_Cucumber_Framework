package Utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Hooks {
	public static WebDriver driver;
	Logger logger = Logger.getLogger("Hooks");
	public Properties prop;

	@Before
	public void openApplication() throws IOException {
		
		prop=new Properties();
		FileInputStream fis=new FileInputStream("src//main//resources//mobile.properties");
		prop.load(fis);
	
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", "OnePlus 7T");
		capabilities.setCapability("udid", "1003af90");
		capabilities.setCapability("platformVersion", "10");
		capabilities.setCapability("platformName", "Android");
		
		String appPackageName=prop.getProperty("appPackageName");
		capabilities.setCapability("appPackage", appPackageName);
		capabilities.setCapability("appActivity", "com.amazon.mShop.home.HomeActivity");
		capabilities.setCapability("autoDismissAlerts", true);
		capabilities.setCapability("autoGrantPermissions", "true");
		
		driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		
		logger.info("Android driver initialized in Hooks");
		logger.info("Amazon app started");
	}

	@After()
	public void tearDown(Scenario scenario) throws IOException {

		driver.quit();
		logger.info("Android driver closed in Hooks");
	}
}
