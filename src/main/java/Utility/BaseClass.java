package Utility;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseClass {
	static Logger logger = Logger.getLogger("BaseClass");
	public static WebDriver driver;
	public static boolean bResult;
	public static Properties prop;

	public BaseClass(WebDriver driver) {
		BaseClass.driver = driver;
		BaseClass.bResult = true;
	}

	public WebElement getElement(WebElement element) {

		WebDriverWait wait = new WebDriverWait(BaseClass.driver, 150);
		wait.until(ExpectedConditions.visibilityOf(element));
		WebElement ele = wait.until(ExpectedConditions.elementToBeClickable(element));
		return ele;
	}

	// This method to be used for the element which frequently gets changed in DOM
	// and gives StaleElementReferenceException
	public boolean retryingClick(WebElement ele) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 20) {
			try {
				//waitForContent();
				retryingFindElement(ele);
				ele.click();
				result = true;
				break;
			} catch (NoSuchElementException e) {

				logger.info("Refinding the element as its throwing NoSuchElementException");

			} catch (ElementClickInterceptedException e) {

				logger.info("Refinding the element as its throwing ElementClickInterceptedException");

			} catch (StaleElementReferenceException e) {

				logger.info("Refinding the element as its throwing StaleElementReferenceException");

			} catch (TimeoutException e) {

				logger.info("Refinding the element as its throwing TimeoutException");

			} catch (Exception e) {

				logger.info("Refinding the element as its throwing exception");
			}
			attempts++;
		}
		return result;
	}

	// This method to be used for the element which frequently gets changed in DOM
	// and gives NoSuchElementException
	public boolean retryingFindElement(WebElement ele) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 20) {
			try {
				//waitForContent();
				ele.isDisplayed();
				result = true;
				break;
			} catch (NoSuchElementException e) {

				logger.info("Refinding the element as its throwing NoSuchElementException");

			} catch (ElementClickInterceptedException e) {

				logger.info("Refinding the element as its throwing ElementClickInterceptedException");

			} catch (StaleElementReferenceException e) {

				logger.info("Refinding the element as its throwing StaleElementReferenceException");

			} catch (TimeoutException e) {

				logger.info("Refinding the element as its throwing TimeoutException");

			} catch (Exception e) {

				logger.info("Refinding the element as its throwing exception");
			}
			attempts++;
		}
		return result;
	}
}
