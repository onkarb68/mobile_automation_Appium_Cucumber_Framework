package PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utility.BaseClass;



/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class HomeScreen extends BaseClass {

	public static WebDriver driver;
	static Logger logger = Logger.getLogger("Homescreen");

	public HomeScreen(WebDriver driver) {

		super(driver);

	}

	@FindBy(how = How.ID, using = "in.amazon.mShop.android.shopping:id/rs_search_src_text")
	public static WebElement searchBar;

	@FindBy(how = How.ID, using = "in.amazon.mShop.android.shopping:id/iss_search_dropdown_item_text")
	public static WebElement firstResult;

}
