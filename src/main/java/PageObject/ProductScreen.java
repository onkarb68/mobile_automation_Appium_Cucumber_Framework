package PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utility.BaseClass;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class ProductScreen extends BaseClass {

	public static WebDriver driver;
	static Logger logger = Logger.getLogger("ProductScreen");
	static BaseClass baseClass = new BaseClass(driver);
	
	public ProductScreen(WebDriver driver) {

		super(driver);

	}

	@FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ViewAnimator/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[4]/android.view.View")
	public static WebElement productID;
	
	public static String getProductName(WebElement ele) {
		WebElement product = baseClass.getElement(ele);
		baseClass.retryingFindElement(product);
		return product.getText();
	}

}
