package PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utility.BaseClass;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class LoginScreen extends BaseClass {

	public static WebDriver driver;
	static Logger logger = Logger.getLogger("Homescreen");

	public LoginScreen(WebDriver driver) {

		super(driver);

	}

	@FindBy(how = How.ID, using = "in.amazon.mShop.android.shopping:id/sso_splash_logo")
	public static WebElement amazonLogo;

	@FindBy(how = How.ID, using = "in.amazon.mShop.android.shopping:id/sso_continue")
	public static WebElement continueButton;

}
