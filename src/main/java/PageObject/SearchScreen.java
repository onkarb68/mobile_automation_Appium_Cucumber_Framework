package PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utility.BaseClass;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class SearchScreen extends BaseClass {

	public static WebDriver driver;
	static Logger logger = Logger.getLogger("Homescreen");

	public SearchScreen(WebDriver driver) {

		super(driver);

	}

	@FindBy(how = How.XPATH, using = "//android.view.View[@content-desc=\"Apple iPhone 11 Pro Max (256GB) - Midnight Green 4.5 out of 5 stars 734 Limited time deal ₹93,900M.R.P.: ₹1,23,900Save ₹30,000 (24%) Currently not deliverable\"]/android.view.View[1]")
	public static WebElement product;
}
