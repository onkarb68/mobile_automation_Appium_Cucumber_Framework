Feature: Search functionality of Amazon

	Scenario Outline: Verify  Search functionality of Amazon till product lanading page
	Given User navigates to Amazon app homescreen
	When User click on continue button to proceed as a default Amazon logged-in user
	And User search for mobile model "<MobileName>"
	And User click on mobile model from search result
	Then User verifies the product details on product landing page
	Examples: 
      | MobileName |
      | Apple iPhone 11 Pro Max (256GB) - Midnight Green  |